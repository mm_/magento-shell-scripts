#!/bin/bash
# Find all Magento Events, include file names, line numbers and the preceding 6 lines of code
 
#Please define these two
ABSOLUTE_PATH_TO_MAGENTO_ROOT=/var/www/git/vinport/magento/
ABSOLUTE_PATH_TO_OUTPUT_FILE_INC_FILE_NAME=~/magentoEvents.txt
 
#here is the command
find $ABSOLUTE_PATH_TO_MAGENTO_ROOT  -name "*.php" | xargs -L10 grep -n -B 6 "dispatchEvent" .> $ABSOLUTE_PATH_TO_OUTPUT_FILE_INC_FILE_NAME
 
# save this file as magentoevents.sh
# then do command: chmod +x magentoevents.sh
# then to run, do command: ./magentoevents.sh
